MINIO_ACCESS_KEY = "admin"
MINIO_SECRET_KEY = "123456789"

# minio server in docker
MINIO_SERVER_HOST = "http://minio:9000"

# minio server (minioadmin/minioadmin)
# MINIO_SERVER_HOST = "http://10.159.37.34:9000"

# kafka server
KAFKA_BOOTSTRAP_SERVERS = "http://10.159.19.59:31090"

HIVE_METASTORE = "thrift://hive-metastore:9083"
