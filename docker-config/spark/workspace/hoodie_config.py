from data_config import UX_TABLE_MAPPINGS

def get_hoodie_config(topic: str, collection_field: str) -> dict:
    """
    return config of hoodie 
    Args:
        topic (str): topic name

    Returns:
        dict: hoodie options
    """

    if collection_field is not None:
        hoodie_options = {
            "hoodie.table.name": f'{topic}',
            "hoodie.metadata.enable": "true",
            "hoodie.table.type": "COPY_ON_WRITE",
            "hoodie.datasource.write.operation": 
                UX_TABLE_MAPPINGS[topic][collection_field]["operation"],
            "hoodie.datasource.write.recordkey.field": 
                UX_TABLE_MAPPINGS[topic][collection_field]["recordkey_field"],
            "hoodie.datasource.write.partitionpath.field": 
                UX_TABLE_MAPPINGS[topic][collection_field]["partitionpath_field"],
            'hoodie.datasource.write.table.name': f'{topic}',
            'hoodie.datasource.write.precombine.field': 
                UX_TABLE_MAPPINGS[topic][collection_field]["precombine"],
            "hoodie.clean.automatic": "true",
            "hoodie.cleaner.policy": "KEEP_LATEST_FILE_VERSIONS",
            "hoodie.cleaner.fileversions.retained": 20,
            "hoodie.compact.inline.max.delta.commits": 2,
            "hoodie.datasource.write.hive_style_partitioning": "true",
            "hoodie.write.concurrency.mode": "optimistic_concurrency_control",
            "hoodie.cleaner.policy.failed.writes": "LAZY",
            "hoodie.write.lock.provider": "org.apache.hudi.client.transaction.lock.ZookeeperBasedLockProvider",
            "hoodie.write.lock.zookeeper.url": "zookeeper:2181",
            "hoodie.write.lock.zookeeper.lock_key": "mykey",
            "hoodie.write.lock.zookeeper.base_path": "/locks-key",
            "hoodie.write.lock.wait_time_ms": "100000",
            "hoodie.write.lock.num_retries": "20",
            # "hoodie.upsert.shuffle.parallelism": "2",
            # "hoodie.insert.shuffle.parallelism": "2",
            "hoodie.datasource.hive_sync.enable": "true",
            "hoodie.datasource.hive_sync.mode": "hms",
            "hoodie.datasource.hive_sync.database": "default",
            "hoodie.datasource.hive_sync.table": f"{collection_field}",
            "hoodie.datasource.hive_sync.partition_fields": 
                UX_TABLE_MAPPINGS[topic][collection_field]["partitionpath_field"],
            "hoodie.datasource.hive_sync.partition_extractor_class":
                "org.apache.hudi.hive.MultiPartKeysValueExtractor",
            "hoodie.datasource.hive_sync.metastore.uris": "thrift://hive-metastore:9083"
        }
    else:
        hoodie_options = {
            "hoodie.table.name": f'{topic}',
            "hoodie.metadata.enable": "true",
            "hoodie.table.type": "COPY_ON_WRITE",
            "hoodie.datasource.write.operation": 
                "UPSERT",
            "hoodie.datasource.write.recordkey.field": 
                UX_TABLE_MAPPINGS[topic]["recordkey_field"],
            "hoodie.datasource.write.partitionpath.field": 
                UX_TABLE_MAPPINGS[topic]["partitionpath_field"],
            'hoodie.datasource.write.table.name': f'{topic}',
            'hoodie.datasource.write.precombine.field': 
                UX_TABLE_MAPPINGS[topic]["precombine"],
            "hoodie.clean.automatic": "true",
            "hoodie.cleaner.policy": "KEEP_LATEST_FILE_VERSIONS",
            "hoodie.cleaner.fileversions.retained": 20,
            "hoodie.compact.inline.max.delta.commits": 2,
            "hoodie.datasource.write.hive_style_partitioning": "true",
            "hoodie.write.concurrency.mode": "optimistic_concurrency_control",
            "hoodie.cleaner.policy.failed.writes": "LAZY",
            "hoodie.write.lock.provider": "org.apache.hudi.client.transaction.lock.ZookeeperBasedLockProvider",
            "hoodie.write.lock.zookeeper.url": "zookeeper:2181",
            "hoodie.write.lock.zookeeper.lock_key": "mykey",
            "hoodie.write.lock.zookeeper.base_path": "/locks-key",
            "hoodie.write.lock.wait_time_ms": "100000",
            "hoodie.write.lock.num_retries": "20",
            "hoodie.upsert.shuffle.parallelism": "2",
            "hoodie.insert.shuffle.parallelism": "2",
            "hoodie.datasource.hive_sync.enable": "true",
            "hoodie.datasource.hive_sync.mode": "hms",
            "hoodie.datasource.hive_sync.database": "default",
            "hoodie.datasource.hive_sync.table": f"{topic}",
            "hoodie.datasource.hive_sync.partition_fields": 
                UX_TABLE_MAPPINGS[topic]["partitionpath_field"],
            "hoodie.datasource.hive_sync.partition_extractor_class":
                "org.apache.hudi.hive.MultiPartKeysValueExtractor",
            "hoodie.datasource.hive_sync.metastore.uris": "thrift://hive-metastore:9083"
        }

    return hoodie_options
