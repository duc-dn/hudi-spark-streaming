from schema.ux_schema import (
    UX_DATA_CLICK_SCHEMA,
    UX_DATA_HEATMAP_MOBILE_IMAGES_SCHEMA,
    UX_DATA_HEATMAP_WEB_IMAGES_SCHEMA,
    UX_DATA_INSERT_LOGS_COLLECTION_SCHEMA,
    UX_DATA_HEATMAP_MOBILE_EVENTS_SCHEMA
)

class UxTableMapping():
    def __init__(
            self, 
            schema,
            partitionpath_field: str = 'year,month,day', 
            recordkey_field: str = '_id', 
            precombine: str = 'timestamp',
            timestamp: str = 'data.timestamp',
            operaton: str = "UPSERT",
            id_col: str = 'data._id'
        ) -> None:

        self.partitionpath_field = partitionpath_field
        self.recordkey_field = recordkey_field
        self.precombine = precombine
        self.schema = schema
        self.operation = operaton
        self.timestamp = timestamp
        self.id_col = id_col

    def get_table_config(self):
        return {
            "partitionpath_field": self.partitionpath_field,
            "recordkey_field": self.recordkey_field,
            "precombine": self.precombine,
            "schema": self.schema,
            "operation": self.operation,
            "timestamp": self.timestamp,
            "id_col": self.id_col
        }

# create hoodie config for each table 
UX_TABLE_MAPPINGS = {
    # "ux_data_exbe": {
    #     "click_events": 
    #         UxTableMapping(schema=UX_DATA_CLICK_SCHEMA).get_table_config(), 
    #     "heatmap_mobile_events": 
    #         UxTableMapping(
    #             schema=UX_DATA_HEATMAP_MOBILE_EVENTS_SCHEMA,
    #             partitionpath_field="device_os",
    #             precombine="query",
    #             timestamp=None
    #         )
    #         .get_table_config(),
    #     "heatmap_mobile_images": 
    #         UxTableMapping(schema=UX_DATA_HEATMAP_MOBILE_IMAGES_SCHEMA)
    #         .get_table_config(),
    #     "heatmap_web_images":
    #         UxTableMapping(schema=UX_DATA_HEATMAP_WEB_IMAGES_SCHEMA)
    #         .get_table_config(),
    # },    
    "ux_data_insert_logs_collection": 
        UxTableMapping(
            schema=UX_DATA_INSERT_LOGS_COLLECTION_SCHEMA,
            timestamp="data.ts",
            id_col="data.h.x-request-id"
        )
        .get_table_config()
}

