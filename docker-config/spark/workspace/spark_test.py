import logging

from pyspark.sql.functions import col, from_json
import pyspark.sql.functions as F
from pyspark.sql import SparkSession
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.types import StringType
from pyspark.sql.streaming import DataStreamReader, StreamingQuery
from pyspark.sql.utils import AnalysisException
from datetime import date

from util.logger import logger
from util.parser_timestamp import parsing_timestamp
from hoodie_config import get_hoodie_config

MINIO_ACCESS_KEY="minioadmin"
MINIO_SECRET_KEY="minioadmin"
MINIO_SERVER_HOST="http://10.159.37.34:9000"

spark = (
        SparkSession.builder.master("local[*]")
        .appName("spark test application")
        .config("spark.sql.extensions", "org.apache.spark.sql.hudi.HoodieSparkSessionExtension")
        .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
        .config("spark.hadoop.fs.s3a.impl",
                "org.apache.hadoop.fs.s3a.S3AFileSystem")
        .config("spark.hadoop.fs.s3a.access.key", MINIO_ACCESS_KEY)
        .config("spark.hadoop.fs.s3a.secret.key", MINIO_SECRET_KEY)
        .config("spark.hadoop.fs.s3a.endpoint", MINIO_SERVER_HOST)
        .config("spark.hadoop.fs.s3a.path.style.access", "true")
        .config("spark.hadoop.fs.s3a.connection.ssl.enabled", "false")
        .config('spark.hadoop.fs.s3a.aws.credentials.provider',
                'org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider')
        .config("spark.hadoop.fs.s3a.connection.maximum", "1000")
        .getOrCreate()
    )


hudi_table_name = "click_events"
df = spark.read.format("hudi").load(f"{hudi_table_name}")

df.createOrReplaceTempView(f"{hudi_table_name}")

spark.sql(f"select count(*) from {hudi_table_name}").show(truncate=False)
